<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->
#  Influxdb Service

* **Responsible Teams**:
  * [infrastructure-observability](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/). **Slack Channel**: [#observability](https://gitlab.slack.com/archives/observability)
* **General Triage Dashboard**: https://dashboards.gitlab.net/d/26q8nTzZz/service-platform-metrics?from=now-6h&to=now&var-prometheus_ds=Global&var-environment=gprd&var-type=influxdb&orgId=1
* **Alerts**: https://alerts.gprd.gitlab.net/#/alerts?filter=%7Btype%3D%22influxdb%22%2C%20tier%3D%22inf%22%7D
* **Label**: gitlab-com/gl-infra/production~"Service:Influxdb"

## Logging

* [system](https://log.gprd.gitlab.net/goto/bf44358a81c549827fd8142a4da59d4a)

## Troubleshooting Pointers

* [manage-influxdb.md](manage-influxdb.md)
* [../monitoring/filesystem_alerts.md](../monitoring/filesystem_alerts.md)
<!-- END_MARKER -->
