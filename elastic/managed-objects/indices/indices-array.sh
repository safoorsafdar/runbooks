#!/bin/bash

declare -a indices

export indices=(
  api
  application
  camoproxy
  consul
  gitaly
  gke
  mailroom
  monitoring
  nginx
  pages
  postgres
  praefect
  puma
  rails
  redis
  registry
  runner
  shell
  sidekiq
  system
  unicorn
  unstructured
  workhorse
)
